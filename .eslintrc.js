module.exports = {
  extends: [
    'airbnb',
    'plugin:flowtype/recommended',
    'prettier',
    'prettier/flowtype',
    'prettier/react',
  ],
  env: {
    browser: true,
  },
  parser: 'babel-eslint',
  plugins: ['flowtype'],
  // Some rules can be too strict. Feel free to relax them. Explain.
  rules: {
    semi: ['error', 'never'],
    'react/jsx-filename-extension': 0, // JavaScript belong to .js
    'react/prop-types': 0, // It's handled by Flow.
    'prefer-destructuring': 0, // Flow casting can need it.
    'import/extensions': 0, // Flow checks it.
    'react/require-default-props': 0, // Not needed with Flow.
  },
}
