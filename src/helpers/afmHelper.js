// @flow
import catalogJson from '../catalog.json'

const afmMeasures = {
  measures: [
    {
      id: '# of Activities',
      definition: {
        baseObject: {
          id: catalogJson.metrics['# of Activities'].identifier,
        },
      },
    },
  ],
}

export const getAfm = (year: number) => {
  const yearDelta = year - new Date().getFullYear()
  return {
    ...afmMeasures,
    filters: [
      {
        between: [yearDelta, yearDelta],
        granularity: 'year',
        id: catalogJson.dateDataSets['Date (Activity)'].identifier,
        intervalType: 'relative',
        type: 'date',
      },
    ],
  }
}

export const getAfmForAllYears = () => ({
  ...afmMeasures,
  attributes: [
    {
      id:
        catalogJson.dateDataSets['Date (Activity)'].attributes[
          'Year (Activity)'
        ].defaultDisplayForm.identifier,
      type: 'attribute',
    },
  ],
})
