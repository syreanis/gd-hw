// @flow
import React, { PureComponent } from 'react'
import '@gooddata/react-components/styles/css/main.css'
import { AfmComponents } from '@gooddata/react-components'
import YearSelect from './components/YearSelect'
import { getAfm, getAfmForAllYears } from './helpers/afmHelper'

const { ColumnChart } = AfmComponents

type AppProps = {}
type AppState = {|
  year: string,
|}

class App extends PureComponent<AppProps, AppState> {
  static yearOptions = [
    {
      value: '2016',
      label: '2016',
    },
    {
      value: '2017',
      label: '2017',
    },
    {
      value: '2018',
      label: '2018',
    },
    {
      value: '2019',
      label: '2019',
    },
  ]
  static initialState = {
    year: '2016',
  }
  state = App.initialState

  handleChange = (e: SyntheticEvent<HTMLSelectElement>) => {
    this.setState({
      year: e.currentTarget.value,
    })
  }

  render() {
    const projectId = process.env.REACT_APP_PROJECT_ID
    const afm = getAfm(parseInt(this.state.year, 10))
    const afmAllYears = getAfmForAllYears()

    return (
      <div className="App">
        <h1>
          # of Activities: Year{' '}
          <YearSelect
            onChange={this.handleChange}
            value={this.state.year}
            options={App.yearOptions}
          />
        </h1>
        <div>
          <ColumnChart afm={afm} projectId={projectId} />
        </div>
        <h1># of Activities: Overview (Year by Year)</h1>
        <div>
          <ColumnChart afm={afmAllYears} projectId={projectId} />
        </div>
      </div>
    )
  }
}

export default App
