// @flow
import React, { PureComponent } from 'react'

type Option = {|
  value: string,
  label: string,
|}

type YearSelectProps = {|
  value: string,
  onChange: (SyntheticEvent<HTMLSelectElement>) => mixed,
  options: Array<Option>,
|}

class YearSelect extends PureComponent<YearSelectProps> {
  render() {
    const { value, onChange, options } = this.props
    return (
      <select value={value} onChange={onChange}>
        {options.map(option => (
          <option value={option.value}>{option.label}</option>
        ))}
      </select>
    )
  }
}

export default YearSelect
